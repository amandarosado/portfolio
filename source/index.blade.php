@extends('_layouts.master')

@section('body')

	<article class="hero homepage-hero">
    <section class="content-wrapper">
      <h1>Perspective is essential to inspiring <span class="text-gradient">action</span></h1>
      <p>I’m Amanda and I’ve spent over 15 years dedicated to creating compelling user experiences through strategy, design and development.</p>
      <a href="/about-me" class="button">Learn About Me <span></span></a>
    </section>
    <a href="#work" id="anchor" class="smooth-scroll"><span>View My Work</span><i class="icon-arrow-down"></i></a>
    
    <div class="shadow-mountain"></div>
    <div class="saturn-esque"></div>

    <div class="starfield">
      <div class="stars slow"></div>
      <div class="stars med"></div>
      <div class="stars fast"></div>
    </div>
  </article>

  @include('_work-templates/work-listing')

@endsection
