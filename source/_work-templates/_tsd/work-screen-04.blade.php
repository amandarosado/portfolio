<figure class="img-wrapper container-lg">
	<img src="/assets/images/work/tsd-work-screen-04-small.png" srcset="/assets/images/work/tsd-work-screen-04-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Homepage Screen</p>
	</figcaption>
</figure>