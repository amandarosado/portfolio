<header>
	<p>New Website - May 2018</p>
	<h1>Tilt Shift Digital</h1>
</header>

<article class="client-description">
	<p>Located in the Hudson Valley of New York, Tilt Shift Digital is a web design and development agency with a focus on transparency and client partnership.</p>
</article>

<article class="my-role">
	<h2>My Role:</h2>
	<p><span class="text-gradient">Content Strategy</span></p>
	<p><span class="text-gradient">UX Design</span></p>
	<p><span class="text-gradient">Web Design</span></p>
	<p><span class="text-gradient">Front-End Development</span></p>
</article>

<article class="challenge">
	<h2>Challenge:</h2>
	<p>When starting our design and development business, my partners and I knew we wanted to be clear about who we were:</p>
	<blockquote>
		<p>We have the experience and skill of a large agency but offer the partnership and accessibility of a small team.</p>
	</blockquote>
	<p>We also wanted our clients to feel a sense of transparency throughout our relationship. These ideas set the foundation for our business model and, ultimately, the structure of our website.</p>
	<p>My first challenge was taking our business goals and services and turning them into a coherent content strategy. I researched many marketing, design and development websites. Most I came across were similar&mdash;an outline of what they do, sample work, and contact information. However, I wanted to figure out a way to stand out a little more. In my research, I came across Simon Sinek’s “Golden Circle” and felt like his method was exactly what I needed to structure the website’s content. </p>
	<p>The second challenge I had was having complete freedom over the design direction. As a starting place, I knew I wanted to focus more on content and less so on an overly designed website. As I researched ideas, I found myself more attracted to minimalist designs with largely white backgrounds and small pops of accent colors.</p>
</article>



