<section class="work-results">
	<div class="content-wrapper container">
	  <h1>Results:</h1>
	  <p><strong>Content Strategy:</strong></p>
    <p>As a starting place, my partners and I created a strategy idea we dubbed the “You Campaign.” We wanted our language to focus on how we make clients comfortable, make them a part of the process, and help them feel like they can put their trust in us to take care of their online presence&mdash;even beyond initial redesigns. With that in mind, I focused the content using the Golden Circle method. First, I figured out the “why” of our company. This came to life with the opening statement of the website:</p>
    <blockquote>
      <p>Understanding people as much as their brands. We don’t just build websites. We build partnerships. Let’s make a site together.</p>
    </blockquote>
    <p>Before even getting into what the business specifically does, I aimed to attract the user with our core goal&mdash;to form partnerships and work together to make something great.</p>
    <p>The next step of the Golden Circle is figuring out the “how” of our company. Further down the homepage, I go into more detail in how the partnership would look:</p>
    <ul>
      <li>It all starts with a conversation.</li>
      <li>We build you a solid foundation.</li>
      <li>Your site launches.</li>
      <li>Upkeep and Maintenance.</li>
      <li>Future Additions and Features.</li>
    </ul>
    <p>These indicate what the process looks like and how we can create a long-term relationship with our clients.</p>
    <p>Finally, the Golden Circle method focuses on the “what.” I include links to our services, process, and sample work through our navigation menu and throughout each page of the website.</p>
    <p><strong>Design Strategy:</strong></p>
    <p>Because I knew I wanted to focus on a minimalist approach to the design, I immediately began thinking about how I would use color. I chose a spectrum of blue shades as the main color. With the darker purple-blue, I wanted to give a sense of professionalism and reliability. With the lighter ocean-blue, I hoped to invoke friendliness and trust. As accent colors, I was drawn to reds and oranges. Combined with the blue shades, I felt red and orange would draw the eye quickly. In the end, I chose a bright orange color, as it has a less of a harsh undertone and gives a feeling of warmth, inviting, and friendliness.</p>
    <p>To add dimension to my minimalist approach, I added geometric shapes throughout the design. The shapes give a sense of stability, symmetry, and balance and help settle a users’ minds as they visit the website.</p>
    <p>I choose Libre Baskerville as the heading font due to its strong, refined nature.  Work Sans was chosen as the paragraph font because of its easy-to-read nature and flexibility. The combination of these fonts felt professional and friendly.</p>
    <p>Finally, I added subtle hover animations to further balance my minimalist approach and sharp iconography highlighted with an orange accent color.</p>
	</div>

  <div class="saturn-esque"></div>

  <div class="starfield">
    <div class="stars slow"></div>
    <div class="stars med"></div>
    <div class="stars fast"></div>
  </div>
</section>