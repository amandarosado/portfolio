<figure class="img-wrapper container-lg">
	<img src="/assets/images/work/tsd-work-screen-03-small.png" srcset="/assets/images/work/tsd-work-screen-03-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Sample Graphics</p>
	</figcaption>
</figure>