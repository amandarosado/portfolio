<figure class="img-wrapper container-lg">
	<img src="/assets/images/work/tsd-work-screen-05-small.png" srcset="/assets/images/work/tsd-work-screen-05-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Services Screen</p>
	</figcaption>
</figure>