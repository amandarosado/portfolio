<figure class="img-wrapper container-lg">
	<img src="/assets/images/work/tsd-work-screen-02-small.png" srcset="/assets/images/work/tsd-work-screen-02-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Typography, color swatches, iconography, hover animations</p>
	</figcaption>
</figure>