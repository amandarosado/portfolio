<figure class="img-wrapper container-lg">
	<img src="/assets/images/work/tsd-work-screen-01-small.png" srcset="/assets/images/work/tsd-work-screen-01-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Evolution of color, content and typography during design strategy</p>
	</figcaption>
</figure>