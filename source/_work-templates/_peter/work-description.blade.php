<header>
	<p>Redesign - June 2017</p>
	<h1>Peter Buffett</h1>
</header>

<article class="client-description">
	<p>For over 30 years, Peter Buffett has left a notable mark as a musician, composer, and author. He is also known as a philanthropist and is cochair of the NoVo Foundation.</p>
</article>

<article class="my-role">
	<h2>My Role:</h2>
	<p><span class="text-gradient">UX Design</span></p>
	<p><span class="text-gradient">Web Design</span></p>
	<p><span class="text-gradient">Front-End Development</span></p>
</article>

<article class="challenge">
	<h2>Challenge:</h2>
	<p>Peter Buffett came to <a href="https://moonfarmer.com/work/peter-buffett" target="_blank">Moonfarmer</a> to refresh his personal website. His website was not particularly outdated, but he was interested in creating a unique experience that would tell his life story. Assigned as the lead creative of the project, I was given the exciting opportunity to experiment how I saw fit and challenge myself by pushing boundaries I had not previously explored.</p>
</article>




