<figure class="img-wrapper container-lg">
	<img src="/assets/images/work/peter-work-screen-01d-small.png" srcset="/assets/images/work/peter-work-screen-01d-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Sample style tile during early concept phase</p>
	</figcaption>
</figure>