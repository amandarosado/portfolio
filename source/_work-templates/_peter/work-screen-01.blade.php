<figure class="img-wrapper container-lg">
	<img src="/assets/images/work/peter-work-screen-01-small.png" srcset="/assets/images/work/peter-work-screen-01-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Music Release Listing Page and Media Library Page wireframes</p>
	</figcaption>
</figure>