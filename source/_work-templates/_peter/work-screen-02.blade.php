<figure class="img-wrapper container-lg">
	<img src="/assets/images/work/peter-work-screen-02-small.png" srcset="/assets/images/work/peter-work-screen-02-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Experiments In Sound Screenshot (with music player open and playing)</p>
	</figcaption>
</figure>