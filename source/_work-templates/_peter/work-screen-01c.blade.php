<figure class="img-wrapper container-md">
	<img src="/assets/images/work/peter-work-screen-01c-small.png" srcset="/assets/images/work/peter-work-screen-01c-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Early design concept for Music Release single page</p>
	</figcaption>
</figure>