<figure class="img-wrapper container-lg">
	<img src="/assets/images/work/peter-work-screen-01b-small.png" srcset="/assets/images/work/peter-work-screen-01b-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Experiments in Sound wireframe</p>
	</figcaption>
</figure>