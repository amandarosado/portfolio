<section class="work-results">
	<div class="content-wrapper container">
	  <h1>Results:</h1>
	  <p>As users explore Peter’s website, they will find themselves on a journey into Peter’s world. It may be witnessing how he evolved over his career or discovering what it’s like for him to experience new melodies and ideas for his songwriting. It could also be seeing an inside look at his performances and how much his upbringing has influenced his life. </p>
	  <p>Listening to Peter’s music and podcasts while browsing the site is yet another way to enhance the experience. Users are given the choice to expand, collapse, play, or pause the player at any time. Nothing plays automatically, which allows for complete control of individual preference.</p>
	  <p>As much as I wanted the user to feel laid back and embrace a bit of chaos, I also wanted to make sure there was still an underlying structure to fall back on. Therefore, I made sure to include the “easy” way to navigate by providing direct links in the modal navigation, footer navigation, and the sidebar music player that require little effort to find.</p>
	  <p>The end result of the website design and structure avoided the idea of a typical “about me” personal site and instead gave the user the opportunity to truly see the world from Peter’s point of view. For me, the experience opened my eyes to new possibilities for how the combination of design and emotion can profoundly affect user experience.</p>
	</div>

  <div class="saturn-esque"></div>

  <div class="starfield">
    <div class="stars slow"></div>
    <div class="stars med"></div>
    <div class="stars fast"></div>
  </div>
</section>