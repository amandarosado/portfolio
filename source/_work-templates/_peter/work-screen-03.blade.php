<figure class="img-wrapper container-lg">
	<img src="/assets/images/work/peter-work-screen-03-small.png" srcset="/assets/images/work/peter-work-screen-03-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Music Release Page Screenshot</p>
	</figcaption>
</figure>