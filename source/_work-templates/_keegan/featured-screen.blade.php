<figure class="img-wrapper">
  <img src="/assets/images/desktop.svg" class="monitor" />
  <img src="/assets/images/work/keegan-featured-small.jpg" srcset="/assets/images/work/keegan-featured-large.jpg 768w" class="lazyload screen">
</figure>