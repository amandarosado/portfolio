<section class="work-results">
	<div class="content-wrapper container">
	  <h1>Results:</h1>
	  <p>Armed with the beautiful photography provided by the client, I incorporated large images as a focus on each page. Rich color combinations, bold typography, and fun hover animations were used to emphasize the spirit of the bar and restaurant. In addition, the homepage focused on the client’s upcoming events by highlighting titles, dates and admission information clearly. A new section was added to the website dedicated to the client’s role in the community.</p>
	</div>

  <div class="saturn-esque"></div>

  <div class="starfield">
    <div class="stars slow"></div>
    <div class="stars med"></div>
    <div class="stars fast"></div>
  </div>
</section>