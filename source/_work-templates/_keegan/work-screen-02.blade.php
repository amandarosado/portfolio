<figure class="img-wrapper container-md">
	<img src="/assets/images/work/keegan-work-screen-02-small.png" srcset="/assets/images/work/keegan-work-screen-02-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Sample design ideas/style tile.</p>
	</figcaption>
</figure>