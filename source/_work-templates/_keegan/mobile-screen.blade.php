<div class="img-container">
	<figure class="img-wrapper">
	  <img src="/assets/images/mobile-wrapper.svg" class="monitor" />
	  <img src="/assets/images/work/keegan-mobile-small.png" srcset="/assets/images/work/keegan-mobile-large.png 768w" class="lazyload screen">
	</figure>
	<footer>
		<a href="http://www.keeganales.com/" class="button" target="_blank">Launch Website <i class="icon-link"></i><span></span></a>
	</footer>
</div>