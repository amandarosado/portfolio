<figure class="img-wrapper container-lg">
	<img src="/assets/images/work/keegan-work-screen-01-small.png" srcset="/assets/images/work/keegan-work-screen-01-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Original wireframes for the Community Page and Homepage. The layout evolved from a sidebar menu into what you see today.</p>
	</figcaption>
</figure>
