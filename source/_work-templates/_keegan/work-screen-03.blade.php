<figure class="img-wrapper container-md">
	<img src="/assets/images/work/keegan-work-screen-03-small.png" srcset="/assets/images/work/keegan-work-screen-03-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Bar & Restaurant Page Screenshot</p>
	</figcaption>
</figure>