<header>
	<p>Redesign - August 2017</p>
	<h1>Keegan Ales</h1>
</header>

<article class="client-description">
	<p>Located in Kingston, NY, Keegan Ales is a local brewery that hosts several events a week and plays a substantial role in the local community. </p>
</article>

<article class="my-role">
	<h2>My Role:</h2>
	<p><span class="text-gradient">UX Design</span></p>
	<p><span class="text-gradient">Web Design</span></p>
	<p><span class="text-gradient">Front-End Development</span></p>
</article>

<article class="challenge">
	<h2>Challenge:</h2>
	<p>Keegan Ales came to <a href="https://moonfarmer.com/work" target="_blank">Moonfarmer</a> looking to refresh their outdated website. They wanted a responsive, fun design and the ability to highlight their events and community involvement in a more effective manner.</p>
</article>



