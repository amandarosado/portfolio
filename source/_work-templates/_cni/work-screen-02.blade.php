<figure class="img-wrapper container-md">
	<img src="/assets/images/work/cni-work-screen-02-small.png" srcset="/assets/images/work/cni-work-screen-02-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Episodes Listing Page Screenshot</p>
	</figcaption>
</figure>