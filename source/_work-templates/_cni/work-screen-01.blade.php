<figure class="img-wrapper container-md">
	<img src="/assets/images/work/cni-work-screen-01-small.png" srcset="/assets/images/work/cni-work-screen-01-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Homepage Screenshot</p>
	</figcaption>
</figure>