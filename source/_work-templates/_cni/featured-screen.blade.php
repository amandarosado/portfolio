<figure class="img-wrapper">
  <img src="/assets/images/desktop.svg" class="monitor" />
  <img src="/assets/images/work/cni-featured-small.jpg" srcset="/assets/images/work/cni-featured-large.jpg 768w" class="lazyload screen">
</figure>