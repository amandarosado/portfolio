<div class="img-container">
	<figure class="img-wrapper">
	  <img src="/assets/images/mobile-wrapper.svg" class="monitor" />
	  <img src="/assets/images/work/cni-mobile-small.png" srcset="/assets/images/work/cni-mobile-large.png 768w" class="lazyload screen">
	</figure>
	<footer>
  	<p><strong>Note:</strong> Site is still in development.</p>
  </footer>
</div>