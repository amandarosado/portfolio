<header>
	<p>Redesign - still in development</p>
	<h1>Comic News Insider</h1>
</header>

<article class="client-description">
	<p>Running for over ten years with a vast collection of guests, Comic News Insider is a weekly podcast that discusses comic books, animation, sci-fi, and related pop culture.</p>
</article>

<article class="my-role">
	<h2>My Role:</h2>
	<p><span class="text-gradient">UX Design</span></p>
	<p><span class="text-gradient">Web Design</span></p>
	<p><span class="text-gradient">Front-End Development</span></p>
</article>

<article class="challenge">
	<h2>Challenge:</h2>
	<p>As a respected podcast, the client wanted to reflect their passion for the material through a modern and fun design. In addition, as the audience has grown over the years, an overhaul of the guest list and episode archive was greatly needed.</p>
</article>


