<figure class="img-wrapper container-md">
	<img src="/assets/images/work/cni-work-screen-03-small.png" srcset="/assets/images/work/cni-work-screen-03-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Guests Listing Page Screenshot</p>
	</figcaption>
</figure>