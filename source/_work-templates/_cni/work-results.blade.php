<section class="work-results">
	<div class="content-wrapper container">
	  <h1>Results:</h1>
	  <p>When designing the site, I wanted to emphasis the fun, casual atmosphere the client believes in. Over a neutral background, I chose bright reds and blues as highlights and headline typography that reflected the source material (and was not Comic Sans!). I also used subtle comic related shapes throughout the website.</p>
    <p>A year and month filter were added to the Episodes page so the user can easily navigate through older content. The Guests page includes a more robust filtering system that allows the user to search episodes by a guest name, topic, or keyword.</p>
	</div>

  <div class="saturn-esque"></div>

  <div class="starfield">
    <div class="stars slow"></div>
    <div class="stars med"></div>
    <div class="stars fast"></div>
  </div>
</section>