<figure class="img-wrapper container-lg">
	<img src="/assets/images/work/tps-work-screen-03-small.png" srcset="/assets/images/work/tps-work-screen-03-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Homepage Screen</p>
	</figcaption>
</figure>