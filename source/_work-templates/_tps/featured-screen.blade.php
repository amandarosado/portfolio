<figure class="img-wrapper">
  <img src="/assets/images/desktop.svg" class="monitor" />
  <img src="/assets/images/work/tps-featured-small.jpg" srcset="/assets/images/work/tps-featured-large.jpg 768w" class="lazyload screen">
</figure>