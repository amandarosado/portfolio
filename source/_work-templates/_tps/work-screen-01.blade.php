<figure class="img-wrapper container-lg">
	<img src="/assets/images/work/tps-work-screen-01-small.png" srcset="/assets/images/work/tps-work-screen-01-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Homepage and Project Single wireframes</p>
	</figcaption>
</figure>