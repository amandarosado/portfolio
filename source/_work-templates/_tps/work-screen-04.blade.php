<figure class="img-wrapper container-lg">
	<img src="/assets/images/work/tps-work-screen-04-small.png" srcset="/assets/images/work/tps-work-screen-04-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Project Single Screen</p>
	</figcaption>
</figure>