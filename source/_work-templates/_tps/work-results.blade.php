<section class="work-results">
	<div class="content-wrapper container">
	  <h1>Results:</h1>
	  <p>The new website eliminated much of the unnecessary navigation links and streamlined user flow. The “Projects” page also gained a filtering system that allowed users to narrow down their choices by category, country and year.</p>
    <p>Bright, earthy colors were chosen to imply feelings of nature and positivity.  Each project highlighted background information, goals, associated donors and a visual timeline of progress.</p>
    <p>To see a full breakdown of my process for this project, view the case study on the Tilt Shift Digital website <a href="https://tiltshiftdigital.com/case-study-the-project-solution/" target="_blank">here</a>.</p>
	</div>

  <div class="saturn-esque"></div>

  <div class="starfield">
    <div class="stars slow"></div>
    <div class="stars med"></div>
    <div class="stars fast"></div>
  </div>
</section>