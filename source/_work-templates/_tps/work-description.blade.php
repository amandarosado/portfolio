<header>
	<p>Redesign - May 2018</p>
	<h1>The Project Solution</h1>
</header>

<article class="client-description">
	<p>The Project Solution is a nonprofit organization with a focus on funding small scale infrastructure projects in rural communities throughout the world. </p>
</article>

<article class="my-role">
	<h2>My Role:</h2>
	<p><span class="text-gradient">UX Design</span></p>
	<p><span class="text-gradient">Web Design</span></p>
	<p><span class="text-gradient">Front-End Development</span></p>
</article>

<article class="challenge">
	<h2>Challenge:</h2>
	<p>The website had not been updated in years, and the client was looking for a complete overhaul in structure and design. They were looking for a fresh, modern look that emphasized the positive energy felt by the team and a special focus on each project they supported.</p>
</article>



