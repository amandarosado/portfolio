<article id="work">
  <ul class="featured-work">
    <li>
      @include('_work-templates/_tsd.featured-screen')
      <section>
        <div class="content-wrapper">
          <header>
            <p>New Website</p>
            <h1>Tilt Shift Digital</h1>
          </header>
          <article>
            <p>Content Strategy, UX Design, Web Design &amp; Front-End Development</p>
            <a href="/work/tilt-shift-digital" class="button"><span class="text">View Project</span> <i class="icon-arrow-right"></i></a>
          </article>
        </div>
        <div class="starfield">
          <div class="stars slow"></div>
          <div class="stars med"></div>
          <div class="stars fast"></div>
        </div>
      </section>
    </li>
    <li>
      @include('_work-templates/_naga.featured-screen')
      <section>
        <div class="content-wrapper">
          <header>
            <p>Redesign</p>
            <h1>Naga Antiques</h1>
          </header>
          <article>
            <p>UX Design, Web Design &amp; Front-End Development</p>
            <a href="/work/naga-antiques" class="button"><span class="text">View Project</span> <i class="icon-arrow-right"></i></a>
          </article>
        </div>
        <div class="starfield">
          <div class="stars slow"></div>
          <div class="stars med"></div>
          <div class="stars fast"></div>
        </div>
      </section>
    </li>
    <li>
      @include('_work-templates/_tbl.featured-screen')
      <section>
        <div class="content-wrapper">
          <header>
            <p>New Website</p>
            <h1>The Beverly Lounge</h1>
          </header>
          <article>
            <p>UX Design, Web Design &amp; Front-End Development</p>
            <a href="/work/the-beverly-lounge" class="button"><span class="text">View Project</span> <i class="icon-arrow-right"></i></a>
          </article>
        </div>
        <div class="starfield">
          <div class="stars slow"></div>
          <div class="stars med"></div>
          <div class="stars fast"></div>
        </div>
      </section>
    </li>
    <li>
      @include('_work-templates/_peter.featured-screen')
      <section>
        <div class="content-wrapper">
          <header>
            <p>Redesign</p>
            <h1>Peter Buffett</h1>
          </header>
          <article>
            <p>UX Design, Web Design &amp; Front-End Development</p>
            <a href="/work/peter-buffett" class="button"><span class="text">View Project</span> <i class="icon-arrow-right"></i></a>
          </article>
        </div>
        <div class="starfield">
          <div class="stars slow"></div>
          <div class="stars med"></div>
          <div class="stars fast"></div>
        </div>
      </section>
    </li>
  </ul>

  <ul class="other-work">
    <li>
      @include('_work-templates/_cni.featured-screen')
      <section class="content-wrapper">
        <header>
          <p>Redesign</p>
          <h1>Comic News Insider</h1>
        </header>
        <article>
          <p>UX Design, Web Design &amp; Front-End Development</p>
          <a href="/work/comic-news-insider" class="button">View Project <i class="icon-arrow-right"></i> <span></span></a>
        </article>
      </section>
    </li>
    <li>
      @include('_work-templates/_kotlikoff.featured-screen')
      <section class="content-wrapper">
        <header>
          <p>Redesign</p>
          <h1>Laurence Kotlikoff</h1>
        </header>
        <article>
          <p>UX Design, Web Design &amp; Front-End Development</p>
          <a href="/work/kotlikoff" class="button">View Project <i class="icon-arrow-right"></i> <span></span></a>
        </article>
      </section>
    </li>
    <li>
      @include('_work-templates/_tps.featured-screen')
      <section class="content-wrapper">
        <header>
          <p>Redesign</p>
          <h1>The Project Solution</h1>
        </header>
        <article>
          <p>UX Design, Web Design &amp; Front-End Development</p>
          <a href="/work/the-project-solution" class="button">View Project <i class="icon-arrow-right"></i> <span></span></a>
        </article>
      </section>
    </li>
    <li>
      @include('_work-templates/_keegan.featured-screen')
      <section class="content-wrapper">
        <header>
          <p>Redesign</p>
          <h1>Keegan Ales</h1>
        </header>
        <article>
          <p>UX Design, Web Design &amp; Front-End Development</p>
          <a href="/work/keegan-ales" class="button">View Project <i class="icon-arrow-right"></i> <span></span></a>
        </article>
      </section>
    </li>
  </ul>
</article>