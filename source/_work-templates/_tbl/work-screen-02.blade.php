<figure class="img-wrapper container-md">
	<img src="/assets/images/work/tbl-work-screen-02-small.png" srcset="/assets/images/work/tbl-work-screen-02-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Design Strategy examples</p>
	</figcaption>
</figure>