<section class="work-results">
	<div class="content-wrapper container">
	  <h1>Results:</h1>
	  <p>To complement the feel of the bar and restaurant, I chose to add a subtle background video to the homepage as an introduction to the user. The combination of the video, masculine textures, and elegant typography distinctly set the tone the client was going for. In addition, I created a simple navigation menu and a dedicated space to highlight upcoming events. Lastly, I took special care to simplify the mobile experience for users on the go.</p>
    <p>To see a full breakdown of my process for this project, view the case study on the Tilt Shift Digital website <a href="https://tiltshiftdigital.com/case-study-the-beverly-lounge/" target="_blank">here</a>.</p>
	</div>

  <div class="saturn-esque"></div>

  <div class="starfield">
    <div class="stars slow"></div>
    <div class="stars med"></div>
    <div class="stars fast"></div>
  </div>
</section>