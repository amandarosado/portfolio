<header>
	<p>New Website - April 2018</p>
	<h1>The Beverly Lounge</h1>
</header>

<article class="client-description">
	<p>A bar and restaurant that opened its doors in August 2017, The Beverly Lounge is located in an area rich in history that dates back to the early 1930s.</p>
</article>

<article class="my-role">
	<h2>My Role:</h2>
	<p><span class="text-gradient">UX Design</span></p>
	<p><span class="text-gradient">Web Design</span></p>
	<p><span class="text-gradient">Front-End Development</span></p>
</article>

<article class="challenge">
	<h2>Challenge:</h2>
	<p>After its debut, owner Trippy Thompson quickly gained traction with the local community but didn’t yet have an online presence to support his business. Armed only with The Beverly Lounge’s logo, he requested a minimal, straightforward user experience with a significant focus on the popular event space and eclectic menu.</p>
	<p>The client took a lot of pride in the restaurant and building history and wanted to emphasize a 1930s “vintage” feel. He was also wanted to capture the dark, gritty textures and mood of the bar.</p>
</article>



