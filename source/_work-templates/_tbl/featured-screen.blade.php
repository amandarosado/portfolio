<figure class="img-wrapper">
  <img src="/assets/images/desktop.svg" class="monitor" />
  <img src="/assets/images/work/tbl-featured-small.jpg" srcset="/assets/images/work/tbl-featured-large.jpg 768w" class="lazyload screen">
</figure>