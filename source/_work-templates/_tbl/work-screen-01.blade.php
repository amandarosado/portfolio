<figure class="img-wrapper container-lg">
	<img src="/assets/images/work/tbl-work-screen-01-small.png" srcset="/assets/images/work/tbl-work-screen-01-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Homepage and Events Listing Page wireframes</p>
	</figcaption>
</figure>