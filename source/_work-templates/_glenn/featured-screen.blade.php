<figure class="img-wrapper">
  <img src="/assets/images/desktop.svg" class="monitor" />
  <img src="/assets/images/work/glenn-featured-small.jpg" srcset="/assets/images/work/glenn-featured-large.jpg 768w" class="lazyload screen">
</figure>