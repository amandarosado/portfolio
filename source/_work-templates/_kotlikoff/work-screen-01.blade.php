<figure class="img-wrapper container-lg">
	<img src="/assets/images/work/kotlikoff-work-screen-01-small.png" srcset="/assets/images/work/kotlikoff-work-screen-01-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Sample Wireframes (including site map and homepage)</p>
	</figcaption>
</figure>
