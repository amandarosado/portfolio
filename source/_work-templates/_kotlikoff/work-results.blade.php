<section class="work-results">
	<div class="content-wrapper container">
	  <h1>Results:</h1>
	  <p>Because the topics Laurence writes about tend to revolve around personal finance, I wanted to choose colors that gave users a sense of welcoming and trust while also appearing professional and knowledgeable&mdash;this resulted in rich blues and minty greens. </p>
    <p>Trying to target the two audiences led me to shy away from a traditional navigation structure. Instead, I created a fixed sidebar menu that tells Laurence’s story. Armed with the titles <em>Read</em>, <em>Act</em>, <em>Connect</em>, users are able to quickly discover who Laurence is and navigate to their destinations with ease.</p>
    <p>To promote Laurence’s companies quickly, we worked together to come up with the phrase “Fix Your Finances” in the navigation as a way to catch the user’s attention and make them curious about what it means. Once on the “Fix Your Finances” page, I chose headlines and button text carefully to increase the chance of conversion.</p>
	</div>

  <div class="saturn-esque"></div>

  <div class="starfield">
    <div class="stars slow"></div>
    <div class="stars med"></div>
    <div class="stars fast"></div>
  </div>
</section>