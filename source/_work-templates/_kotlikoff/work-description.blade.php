<header>
	<p>Redesign - May 2019</p>
	<h1>Laurence Kotlikoff</h1>
</header>

<article class="client-description">
	<p>An economics professor at Boston University, Laurence Kotlikoff sought a presidential run in 2012, is a <em>New York Times</em> best selling author, and is president of Economic Security Planning, Inc.</p>
</article>

<article class="my-role">
	<h2>My Role:</h2>
	<p><span class="text-gradient">UX Design</span></p>
	<p><span class="text-gradient">Web Design</span></p>
	<p><span class="text-gradient">Front-End Development</span></p>
</article>

<article class="challenge">
	<h2>Challenge:</h2>
	<p>Though it was a given that the site needed a general design refresh, Laurence’s main concerns were making sure users had easy access to his articles and the ability to promote his company websites. The navigation structure was also somewhat confusing and needed rearranging.</p>
	<p>An additional challenge to overcome was how to grab the attention of two audiences&mdash;people interested in personal finance and academics looking to read his work.</p>
</article>


