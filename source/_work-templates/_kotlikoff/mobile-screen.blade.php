<div class="img-container">
	<figure class="img-wrapper">
	  <img src="/assets/images/mobile-wrapper.svg" class="monitor" />
	  <img src="/assets/images/work/kotlikoff-mobile-small.png" srcset="/assets/images/work/kotlikoff-mobile-large.png 768w" class="lazyload screen">
	</figure>
	<footer>
		<a href="https://kotlikoff.net/" class="button" target="_blank">Launch Website <i class="icon-link"></i><span></span></a>
	</footer>
</div>