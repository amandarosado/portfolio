<figure class="img-wrapper container-md">
	<img src="/assets/images/work/kotlikoff-work-screen-02-small.png" srcset="/assets/images/work/kotlikoff-work-screen-02-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Prepped Style Guide</p>
	</figcaption>
</figure>