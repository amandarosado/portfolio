<div class="img-container">
	<figure class="img-wrapper">
	  <img src="/assets/images/mobile-wrapper.svg" class="monitor" />
	  <img src="/assets/images/work/naga-mobile-small.png" srcset="/assets/images/work/naga-mobile-large.png 768w" class="lazyload screen">
	</figure>
	<footer>
		<p><strong>Note:</strong> Site is still in development.</p>
		{{-- <a href="https://nagaantiques.com/" class="button" target="_blank">Launch Website <i class="icon-link"></i><span></span></a> --}}
	</footer>
</div>