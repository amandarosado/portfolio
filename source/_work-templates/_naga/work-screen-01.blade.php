<figure class="img-wrapper container-lg">
	<img src="/assets/images/work/naga-work-screen-01-small.png" srcset="/assets/images/work/naga-work-screen-01-large.png 768w" class="lazyload screen">
	<figcaption>
		<p>Sample Wireframes (including site map and product single page)</p>
	</figcaption>
</figure>
