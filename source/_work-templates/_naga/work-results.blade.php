<section class="work-results">
	<div class="content-wrapper container">
	  <h1>Results:</h1>
	  <p>Because the main focus of the website was to be browsing through a collection of products, I felt it was important not to distract users with an overly designed website. Armed with a solid white background and one accent color used sporadically, the user cannot help but be drawn to product images rich in color.</p><p>The client has a vast collection of products so I created a separate modal navigation menu that displays all the parent categories users would find most relevant. After choosing a relevant category users can then browse related products. The ability to choose a “grid layout” view or “listing layout” view gives them complete control on how they want to browse.</p>
	</div>

  <div class="saturn-esque"></div>

  <div class="starfield">
    <div class="stars slow"></div>
    <div class="stars med"></div>
    <div class="stars fast"></div>
  </div>
</section>