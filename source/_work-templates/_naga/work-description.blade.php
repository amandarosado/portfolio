<header>
	<p>Redesign - August 2019</p>
	<h1>Naga Antiques</h1>
</header>

<article class="client-description">
	<p>Located in Hudson, NY, Naga Antiques is one of the world’s leading dealers for Japanese screens, unique works of art, fine furniture, and more.</p>
</article>

<article class="my-role">
	<h2>My Role:</h2>
	<p><span class="text-gradient">UX Design</span></p>
	<p><span class="text-gradient">Web Design</span></p>
	<p><span class="text-gradient">Front-End Development</span></p>
</article>

<article class="challenge">
	<h2>Challenge:</h2>
	<p>The client felt their website was dated and hard to navigate. They were interested in creating a modern, responsive, and professional redesign that would greatly enhance the focus on their products. In addition, they hoped to create a better browsing experience, and to simplify the process of inquiring about individual products.</p>
</article>