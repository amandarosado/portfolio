@extends('_layouts.work')

@section('body')
	

	<article class="work-hero">

    @include('_work-templates/_tbl.featured-screen')

    <div class="starfield">
      <div class="stars slow"></div>
      <div class="stars med"></div>
      <div class="stars fast"></div>
    </div>
  </article>

  <article class="work-description">
  	<div class="container">
	  	<section class="content-wrapper">
	  		@include('_work-templates/_tbl.work-description')
	  	</section>
	  	@include('_work-templates/_tbl.mobile-screen')
	  </div>
  </article>

  @include('_work-templates/_tbl.work-results')

  <article class="work-screens">
    @include('_work-templates/_tbl.work-screen-01')
    @include('_work-templates/_tbl.work-screen-02')
  	@include('_work-templates/_tbl.work-screen-03')
    @include('_work-templates/_tbl.work-screen-04')

  	<div class="starfield">
      <div class="stars slow"></div>
      <div class="stars med"></div>
      <div class="stars fast"></div>
    </div>
  </article>

  <ul class="paging">
  	<li class="prev"><a href="/work/naga-antiques"><span>Prev Project</span> <i class="icon-arrow-right"></i></a></li>
    <li class="next"><a href="/work/peter-buffett"><span>Next Project</span> <i class="icon-arrow-right"></i></a></li>
  </ul>

@endsection