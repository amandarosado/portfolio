@extends('_layouts.work')

@section('body')
	

	<article class="work-hero">

    @include('_work-templates/_kotlikoff.featured-screen')

    <div class="starfield">
      <div class="stars slow"></div>
      <div class="stars med"></div>
      <div class="stars fast"></div>
    </div>
  </article>

  <article class="work-description">
  	<div class="container">
	  	<section class="content-wrapper">
	  		@include('_work-templates/_kotlikoff.work-description')
	  	</section>
	  	@include('_work-templates/_kotlikoff.mobile-screen')
	  </div>
  </article>

  @include('_work-templates/_kotlikoff.work-results')

  <article class="work-screens">
  	@include('_work-templates/_kotlikoff.work-screen-01')
  	@include('_work-templates/_kotlikoff.work-screen-02')
  	@include('_work-templates/_kotlikoff.work-screen-03')

  	<div class="starfield">
      <div class="stars slow"></div>
      <div class="stars med"></div>
      <div class="stars fast"></div>
    </div>
  </article>

  <ul class="paging">
  	<li class="prev"><a href="/work/comic-news-insider"><span>Prev Project</span> <i class="icon-arrow-right"></i></a></li>
    <li class="next"><a href="/work/the-project-solution"><span>Next Project</span> <i class="icon-arrow-right"></i></a></li>
  </ul>

@endsection