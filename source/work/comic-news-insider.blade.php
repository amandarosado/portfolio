@extends('_layouts.work')

@section('body')
	

	<article class="work-hero">

    @include('_work-templates/_cni.featured-screen')

    <div class="starfield">
      <div class="stars slow"></div>
      <div class="stars med"></div>
      <div class="stars fast"></div>
    </div>
  </article>

  <article class="work-description">
  	<div class="container">
	  	<section class="content-wrapper">
	  		@include('_work-templates/_cni.work-description')
	  	</section>
	  	@include('_work-templates/_cni.mobile-screen')
	  </div>
  </article>

  @include('_work-templates/_cni.work-results')

  <article class="work-screens">
  	@include('_work-templates/_cni.work-screen-01')
    @include('_work-templates/_cni.work-screen-02')
    @include('_work-templates/_cni.work-screen-03')
    @include('_work-templates/_cni.work-screen-04')
    @include('_work-templates/_cni.work-screen-05')

  	<div class="starfield">
      <div class="stars slow"></div>
      <div class="stars med"></div>
      <div class="stars fast"></div>
    </div>
  </article>

  <ul class="paging">
  	<li class="prev"><a href="/work/peter-buffett"><span>Prev Project</span> <i class="icon-arrow-right"></i></a></li>
    <li class="next"><a href="/work/kotlikoff"><span>Next Project</span> <i class="icon-arrow-right"></i></a></li>
  </ul>

@endsection