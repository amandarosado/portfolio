@extends('_layouts.work')

@section('body')
	

	<article class="work-hero">

    @include('_work-templates/_tps.featured-screen')

    <div class="starfield">
      <div class="stars slow"></div>
      <div class="stars med"></div>
      <div class="stars fast"></div>
    </div>
  </article>

  <article class="work-description">
  	<div class="container">
	  	<section class="content-wrapper">
	  		@include('_work-templates/_tps.work-description')
	  	</section>
	  	@include('_work-templates/_tps.mobile-screen')
	  </div>
  </article>

  @include('_work-templates/_tps.work-results')

  <article class="work-screens">

    @include('_work-templates/_tps.work-screen-01')
    @include('_work-templates/_tps.work-screen-02')
    @include('_work-templates/_tps.work-screen-03')
    @include('_work-templates/_tps.work-screen-04')

  	<div class="starfield">
      <div class="stars slow"></div>
      <div class="stars med"></div>
      <div class="stars fast"></div>
    </div>
  </article>

  <ul class="paging">
  	<li class="prev"><a href="/work/kotlikoff"><span>Prev Project</span> <i class="icon-arrow-right"></i></a></li>
    <li class="next"><a href="/work/keegan-ales"><span>Next Project</span> <i class="icon-arrow-right"></i></a></li>
  </ul>

@endsection