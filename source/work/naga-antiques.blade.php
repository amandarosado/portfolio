@extends('_layouts.work')

@section('body')
	

	<article class="work-hero">

    @include('_work-templates/_naga.featured-screen')

    <div class="starfield">
      <div class="stars slow"></div>
      <div class="stars med"></div>
      <div class="stars fast"></div>
    </div>
  </article>

  <article class="work-description">
  	<div class="container">
	  	<section class="content-wrapper">
	  		@include('_work-templates/_naga.work-description')
	  	</section>
	  	@include('_work-templates/_naga.mobile-screen')
	  </div>
  </article>

  @include('_work-templates/_naga.work-results')

  <article class="work-screens">
  	@include('_work-templates/_naga.work-screen-01')
  	@include('_work-templates/_naga.work-screen-02')
  	@include('_work-templates/_naga.work-screen-03')

  	<div class="starfield">
      <div class="stars slow"></div>
      <div class="stars med"></div>
      <div class="stars fast"></div>
    </div>
  </article>

  <ul class="paging">
  	<li class="prev"><a href="/work/tilt-shift-digital"><span>Prev Project</span> <i class="icon-arrow-right"></i></a></li>
    <li class="next"><a href="/work/the-beverly-lounge"><span>Next Project</span> <i class="icon-arrow-right"></i></a></li>
  </ul>

@endsection