@extends('_layouts.master')

@section('body')

	<article class="about-hero">
    <section class="content-wrapper">
      <h1>About Me</h1>
      <p>My name is Amanda Postle Rosado and I’m a <span class="text-gradient">web designer</span> and <span class="text-gradient">front-end developer</span> living in the Hudson Valley of New York. </p>
    </section>
    <figure class="img-wrapper">
      <img src="/assets/images/portrait.jpg" alt="Amanda L. Postle Rosado" />
    </figure>
    
    <div class="saturn-esque"></div>

    <div class="starfield">
      <div class="stars slow"></div>
      <div class="stars med"></div>
      <div class="stars fast"></div>
    </div>
  </article>

  <article class="about-description">
    <h1>For the past 15 years, it has been my <span class="text-gradient">pleasure</span> to create interesting, effective user experiences through <span class="text-gradient">critical thinking</span>, <span class="text-gradient">emotional</span> connection and <span class="text-gradient">meticulous</span> code.</h1>
    <a href="/work" class="button">View My Work <i class="icon-arrow-right"></i><span></span></a>
  </article>

  <article class="about-expertise">
    <div class="container">
      <header>
        <h1>Expertise</h1>
        <a href="/assets/images/work/Resume.pdf" class="button" target="_blank"><span>Resume</span> <i class="icon-file-pdf"></i><span></span></a>
      </header>

      <section class="skills-listing">
        <ul>
          <li>
            <h2>Design Prototyping</h2>
            <p>Sketch, </p>
            <p>Photoshop, </p>
            <p>Illustrator, </p>
            <p>InDesign, </p>
            <p>Principle, </p>
            <p>Invision/Marvel</p>
          </li>
          <li>
            <h2>Development Tools</h2>
            <p>NPM, </p>
            <p>Yarn, </p>
            <p>Gulp, </p>
            <p>Grunt, </p>
            <p>Webpack</p>
          </li>
          <li>
            <h2>Frameworks</h2>
            <p>jQuery, </p>
            <p>Larvael's Blade, </p>
            <p>Jigsaw, </p>
            <p>Sage Wordpress Starter Theme</p>
          </li>
          <li>
            <h2>Content Management</h2>
            <p>Wordpress, </p>
            <p>Expression Engine</p>
          </li>
          <li>
            <h2>Web Languages</h2>
            <p>HTML5, </p>
            <p>CSS3, </p>
            <p>SASS</p>
          </li>
          <li>
            <h2>Version Control</h2>
            <p>Git, </p>
            <p>Bitbucket, </p>
            <p>GitLab</p>
          </li>
        </ul>
      </section>
    </div>
    <div class="starfield">
      <div class="stars slow"></div>
      <gdiv class="stars med"></div>
      <div class="stars fast"></div>
    </div>
  </article>

  <article class="about-employment">
    <div class="container">
      <figure class="img-wrapper">
        <img src="/assets/images/cards.svg" />
      </figure>
      <section class="content-wrapper">
        <h1>Past Employment</h1>
        <ul>
          <li>Partner &amp; Web Designer, <a href="https://tiltshiftdigital.com/" target="_blank">Tilt Shift Digital</a>, 2017-2019</li>
          <li>Senior Front-End Developer & Designer, <a href="https://moonfarmer.com/" target="_blank">Moonfarmer</a>, 2014-2017</li>
          <li>Front-End Developer, <a href="https://www.bostondigital.com/" target="_blank">Boston Interactive</a>, 2012-2014</li>
          <li>Web Designer, <a href="https://www.webershandwick.com/" target="_blank">Weber Shandwick</a>, 2007-2012</li>
          <li>Web Master, <a href="https://makingmusicmag.com/" target="_blank">Making Music Magazine</a>, 2005-2007</li>
          <li>Web Designer Intern, <a href="https://beckysgraphicdesign.com/" target="_blank">Becky’s Graphic Design</a>, 2004</li>
        </ul>
      </section>
      
    </div>
  </article>

  <article class="about-random">
    <div class="container">
      <h1>9 Random Facts about Me</h1>
    </div>
    <ol>
      <li>
        <span class="number">01</span>
        <p>I'm left-handed.</p>
      </li>
      <li>
        <span class="number">02</span>
        <p>I have a beautiful (almost) 4 year old daughter named Evie.</p>
      </li>
      <li>
        <span class="number">03</span>
        <p>If I could only eat one thing for the rest of my life it'd be a Wegmans sub.</p>
      </li>
      <li>
        <span class="number">04</span>
        <p>At 17, I created my first website in 2000 and hosted it on Geocities. It was a fan page dedicated to The X-Files.</p>
      </li>
      <li>
        <span class="number">05</span>
        <p>I played the Bassoon for many years.</p>
      </li>
      <li>
        <span class="number">06</span>
        <p>I've been playing video games since I was a kid. Mass Effect, Dragon Age and Tombraider are among my favorites.</p>
      </li>
      <li>
        <span class="number">07</span>
        <p>Before switching to web design as a career, I was studying to become an astronomer.</p>
      </li>
      <li>
        <span class="number">08</span>
        <p>My favorite book series is The Sword of Truth.</p>
      </li>
      <li>
        <span class="number">09</span>
        <p>Captain Picard is my hero.</p>
      </li>
    </ol>

    <div class="starfield">
      <div class="stars slow"></div>
      <div class="stars med"></div>
      <div class="stars fast"></div>
    </div>
  </article>

@endsection