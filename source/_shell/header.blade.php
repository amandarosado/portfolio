<header class="site-header">
    <a href="/" class="logo"><span>Amanda L. Postle Rosado</span><img src="/assets/images/logo.svg" alt="Amanda L. Postle Rosado" /></a>
    <button id="toggle-nav">
      <div class="lines">
        <span class="line"></span>
        <span class="line"></span>
        <span class="line"></span>
        <span class="line"></span>
      </div>
    </button>
</header>

<nav class="site-navigation">
  <div class="content-wrapper">
    <ul>
      <li><a href="/work">Work</a></li>
      <li><a href="/about-me">About Me</a></li>
      {{-- <li><a href="/my-approach">My Approach</a></li> --}}
      <li><a href="mailto:amanda@alprdesign.com" class="button">Send Me an Email <span></span></a></li>
    </ul>
  </div>
</nav>