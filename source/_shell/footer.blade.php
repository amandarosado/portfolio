<footer class="site-footer">
  <div class="container">
  	<ul>
  		<li>&copy; Amanda L. Postle Rosado, 2000-<?php echo date("Y"); ?> </li>
  		<li><a href="mailto:amanda@alprdesign.com">Send Me an Email</a></li>
  	</ul>
  </div>
</footer>