@extends('_layouts.master')

@section('body')

	<article class="work-listing-hero">
    <section class="content-wrapper container">
      <h1>Recent Work</h1>
    </section>
    
    <div class="saturn-esque"></div>

    <div class="starfield">
      <div class="stars slow"></div>
      <div class="stars med"></div>
      <div class="stars fast"></div>
    </div>
  </article>

  @include('_work-templates/work-listing')

@endsection