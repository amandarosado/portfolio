<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Amanda Postle Rosado Portfolio</title>
        <link rel="stylesheet" href="{{ mix('css/main.css', 'assets/build') }}">
        <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,300i,600,600i,700|Raleway:500,600&display=swap" rel="stylesheet">
    </head>
    <body class="work-single">
    	@include('_shell.header')
        <main class="site-document">
            @yield('body')
        </main>
        @include('_shell.footer')
        <script src="/assets/build/js/main.js"></script>
    </body>
</html>