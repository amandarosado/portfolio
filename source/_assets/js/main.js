// import external dependencies
import jquery from 'jquery';
window.jQuery = jquery;
window.$ = jquery;

// Import Lazy Loading Images
import 'lazysizes';

$(document).ready(function(){

	// Site Navigation
	$('#toggle-nav').click(function(e) {
    e.preventDefault();
    $(this).toggleClass('open-nav');
    $('body').toggleClass('noscroll nav-is-open');
  });

	// Smooth Scrolling
  $('.smooth-scroll').on('click', function(e){
    e.preventDefault();
    $('html, body').animate({scrollTop: $(this.hash).offset().top}, 600);
    return false;
  });

  // SUCCESSFULL Smooth Scrolling from another page
  $('html, body').hide();

  if (window.location.hash) {
      setTimeout(function() {
          $('html, body').scrollTop(0).show();
          $('html, body').animate({
              scrollTop: $(window.location.hash).offset().top
              }, 1000);
      }, 0);
  }
  else {
      $('html, body').show();
  }
});